<?php

require_once './DB/PDOConnect.class.php';
require_once './config/database.php';
require_once './DB/CrudPDO.php';
require_once './Models/Produto.php';

$conn = new PDOConnect();
$conn = $conn->connect();
$produto = new Produto();


//var_dump($produto->get());

//echo json_encode($produto->getJson());
if($produto->insert(['cod'=> $_POST['cod'], 'nome'=> $_POST['nome']])){
    echo json_encode(['resultValue'=>'1', 'resultText'=>'Produto Cadastrado']);
}else{
    echo json_encode(['resultValue'=>'0', 'resultText'=>'Produto Não Cadastrado']);
}