<!DOCTYPE html>
<html>
	<meta charset="UTF-8">
	<head>
		<title>Curso de Ajax - EspecializaTi</title>

		<!--Bootstrap CSS-->
		<link rel="stylesheet" href="css/bootstrap.css">

		<link rel="stylesheet" href="css/style.css">
	</head>
<body>

<div class="container">
	<div class="text-center">
		<img src="imgs/especializati-logo.png" alt="EspecializaTi" class="logo">
	</div>
	
	<a class="btn-add" href="#" data-toggle="modal" data-target="#modalGestaoEspecializaTi">
		<img src="imgs/add.png" alt="Add" class="icon-image">
	</a>

	<a class="btn-refresh" href="#">
		<img src="imgs/refresh.png" alt="Add" class="icon-image">
	</a>

	<a href="#" class="btn btn-primary getjson">JSON</a>

	<a href="#" class="btn btn-default ajaxget">GET</a>

	<a href="#" class="btn btn-info ajaxpost">POST</a>

	<a href="#" class="btn btn-warning ajax">AJAX</a>

	<div class="preloader" style="display: none;">
		<p>Carregando os dados <img src="imgs/load.gif"></p>
	</div>

	<table class="table table-hover">
		<tr>
			<th>COD.</th>
			<th>Nome</th>
			<th width="100px" align="center">Ações</th>
		</tr>
               	
	</table>

</div>


<!-- Modal -->
<div class="modal fade" id="modalGestaoEspecializaTi" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-especializati">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Gestão Produto</h4>
      </div>
      <div class="modal-body">
        <form method="post" action="#" id="form-gestao">
        	<div class="alert alert-success" role="alert" style="display:none"></div>
			<div class="alert alert-warning" role="alert" style="display:none"></div>

        	<div class="form-group">
        		<input type="text" name="cod"  placeholder="Código do Produto">
        	</div>
        	<div class="form-group">
        		<input type="text" name="nome" placeholder="Nome do Produto">
        	</div>
        	        
        	<div class="form-group">
        		<div class="preloader-form" style="display: none;">
                        	<p>Enviando dados do formulário... <img src="imgs/load.gif"></p>
			</div>
        	</div>
      </div>
      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="submit" class="btn btn-primary">Salvar</button>
        </form>
      </div>
    </div>
  </div>
</div>

<!-- Modal Delete-->
<div class="modal fade" id="modalConfirmacaoDeletar" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header modal-header-delete">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Deletar?</h4>
      </div>
      <div class="modal-body">
        Deseja realmente deletar?
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
        <button type="submit" class="btn btn-danger">Deletar</button>
      </div>
    </div>
  </div>
</div>

<script src="js/jquery-3.1.1.min.js"></script>
	<script src="js/bootstrap.min.js"></script>


	<script>
		//Inicio do jQuery
		$(function(){
                    listData();
                    //RETORNA APENAS JSON
                    jQuery(".getjson").click(function(){
                        jQuery.getJSON("getJson.php",function(data){
                            var tdLink = '<td><a href="#" class="edit" data-toggle="modal" data-target="#modalGestaoEspecializaTi"><img src="imgs/edit.png" alt="Edit" class="icon-image"></a><a href="#" class="delete" data-toggle="modal" data-target="#modalConfirmacaoDeletar"><img src="imgs/delete.png" alt="Delete" class="icon-image"></a></td>';
                            jQuery.each(data, function(key, value){
                                //RETORNA O NOME DO INDEXADOR
                                //alert(value.cod);
                                //alert(value.nome);
                                
                                jQuery(".table-hover").append("<tr><td>"+value.cod+"</td><td>"+value.nome+"</td>"+tdLink+"</tr>");
                            });
                        });
                    });
                    
                    //FAZ UMA REQUISIÇÃO VIA GET COM UM PARAMETRO
                    jQuery(".ajaxget").click(function(){
                        jQuery.get("getGET.php?param1=Abacaxi", function(data){
                           alert(data); 
                        });
                    });
                    
                    //FAZ UMA REQUISIÇÃO VIA POST PASSANDO PARAMETROS
                    jQuery(".ajaxpost").click(function(){
                        jQuery.post("getPOST.php", {nome:"Kako", sobrenome:"Ferrare"}, function(data){
                            alert(data);
                        });
                    });
                    
                    //FAZ UMA REQUISIÇÃO VIA AJAX PASSANDO PARAMETROS
                    jQuery(".ajax").click(function(){
                        var dados = {"nome":"Kako","sobrenome":"Ferrare"};
                        jQuery.ajax({
                            url: 'ajax.php',
                            data: dados,
                            //data:{"nome":"Kako","sobrenome":"Ferrare"}
                            //NÃO É OBRIGATÓRIO, MAS SE NÃO PASSAR VAI SER VIA GET POR PADRÃO
                            method: 'POST',
                            beforeSend: preLoad(),
                            complete: preLoadOut()
                        }).done(function(data){
                            alert(data);
                        }).fail(function(){
                            alert("Falha na requisição");
                        });
                    });
                    
                    
		});
                
                function preLoad(){
                    jQuery('.preloader').fadeIn(1000);
                };
                
                function preLoadFormulario(){
                    jQuery('.preloader-form').fadeIn(1500);
                };
                
                function preLoadFormularioOut(){
                    jQuery('.preloader-form').fadeOut(1500);
                };
                
                function preLoadOut(){
                    jQuery('.preloader').fadeOut(1000);
                };
                
                jQuery(".btn-refresh").click(function(){
                   listData(); 
                });
                
                jQuery("#form-gestao").submit(function(e){
                        e.preventDefault();
                        //var dados = {cod: jQuery("input[name=cod]").val(), nome: jQuery("input[name=nome]").val()};
                        //SERIALIZE PEGA TODOS OS DADOS DO FORMULÁRIO
                        var dados = jQuery(this).serialize();
                        jQuery.ajax({
                            url:"gestaoProduto.php",
                            method:"POST",
                            dataType : 'json',
                            data: dados,
                            beforeSend: preLoadFormulario(),
                            complete: function(response){
                                console.log(response);
                                preLoadFormularioOut();
                            if(response.responseJSON.resultValue === "1"){
                                jQuery(".alert-success").html(response.responseJSON.resultText).fadeIn(1000);
                                setTimeout("jQuery('.alert-success').fadeOut(1000); jQuery('#modalGestaoEspecializaTi').modal('hide');", 3000);
                                setTimeout("listData();", 4000);
                            }
                            
                            else{
                                jQuery(".alert-warning").html(response.responseJSON.resultText).fadeIn(1000);
                                setTimeout("jQuery('.alert-warning').fadeOut(1000);", 2000);
                            }
                            },
                            fail:function(response){
                             jQuery(".alert-warning").html("Erro na requisição").fadeIn(1000);
                             setTimeout("jQuery('.alert-warning').fadeOut(1000);", 3000);
                            }
                    
                        });
                    });
                    
                    function listData(){
                        jQuery("tbody tr:gt(0)").empty();
                        jQuery.getJSON("listProdutos.php",function(data){
                                var tdLink = '<td><a href="#" class="edit" data-toggle="modal" data-target="#modalGestaoEspecializaTi"><img src="imgs/edit.png" alt="Edit" class="icon-image"></a><a href="#" class="delete" data-toggle="modal" data-target="#modalConfirmacaoDeletar"><img src="imgs/delete.png" alt="Delete" class="icon-image"></a></td>';
                                jQuery.each(data, function(key, value){
                                    jQuery("tbody").append("<tr><td>"+value.cod+"</td><td>"+value.nome+"</td>"+tdLink+"</tr>");
                                });
                            });
                    }
                    
                    jQuery(document).ajaxStart(function(){
                        //alert("Algum ajax iniciado");
                        jQuery(".preloader").fadeIn(1000);
                    }).ajaxStop(function(){
                        //alert("Algum ajax finalizado");
                        jQuery(".preloader").fadeOut(1000);
                    });
             
	</script>
</body>
</html>