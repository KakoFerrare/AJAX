<?php
/**
 * Classe de CRUD PDO para facilitar a interação com o banco de dados, de maneira profissional
 * 
 * @author Kako Ferrare <kakoferrare87@gmail.com>
 * @copyright (c) 2016, Kako Ferrare(Empresa)
 * @version 1
 */

class CrudPDO {

    //1 NO WHERE PORQUE QUANDO PASSO 1 ELE RETORNA UM SELECT NORMAL
    private  $conn, $select = '*', $where = '1', $cond, $join, $orderBy, $limit, $distinct, $total;
    
    
    
    public function __construct() {
        $this->conn = PDOConnect::connect();
    }
    
    /*public function lists(){
        $data = $this->conn->query("Select * from {$this->table}");
        return $data->fetchAll(PDO::FETCH_OBJ);
    }*/
    
    
    /**
     * Recupera um registro pelo seu id
     * 
     * @param int $id - Id do registro a ser recuperado
     * @return Objects
     */
    public function find($id){
        $data = $this->conn->prepare("Select {$this->select} from {$this->table} where id = :id");
        $data->bindValue(':id', $id, PDO::PARAM_INT);
        $data->execute();
        return $data->fetch(PDO::FETCH_OBJ);
    }
    
    /**
     * Insere um array dados no banco de dados
     *  
     * @param array $dados - Dados a serem inseridos
     * @return boolean
     */
    public function insert(array $dados){
        $fields = implode(',', array_values($this->fields));
        $keys = ':'.implode(', :', array_values($this->fields));
        
        $insert = $this->conn->prepare("Insert into {$this->table} ({$fields}) values ({$keys})");
        foreach ($this->fields as $value){
            $insert->bindValue(":{$value}", $dados[$value]);
        }
        return $insert->execute();
    }

    /**
     * Insere um array editando um registro no banco de dados
     * 
     * @param type $id - Id do objeto a ser editado
     * @param array $dados - Dados do item a ser editado
     * @return boolean
     */
    public function update($id, array $dados){
        $fields = array();
        foreach ($this->fields as $value){
            $fields[]= "$value=:$value";
        }
        
        $fields = implode(', ', $fields);
        $update = $this->conn->prepare("Update {$this->table} set {$fields} where id=:id");
        $update->bindValue(':id', $id, PDO::PARAM_INT);
        foreach ($this->fields as $value){
        $update->bindValue(":{$value}", $dados[$value]);
        }
        return $update->execute(); 
    }
    
    /**
     * Deleta um registro no banco de dados
     * 
     * @param type $id - Id do registro a ser deletado
     * @return boolean
     */
    public function delete($id){
        $delete = $this->conn->prepare("Delete from {$this->table} where id = :id");
        $delete->bindValue(':id', $id, PDO::PARAM_INT);
        return $delete->execute();
    }
    
    /**
     * Define quais colunas mostrar
     * Por padrão mostra *
     * 
     * @param string $select - Colunas
     * @return \CrudPDO
     */
    public function select($select = '*'){
        $this->select = $select;
        return $this;
    }
    
    /**
     * 
     * FAZ A CLAUSULA DO WHERE DA QUERY EX: where(turmas.id, 1)
     * 
     * @param string $where Nome Coluna
     * @param string $cond Condição da Query
     * @return \CrudPDO
     */
    public function where($where, $cond){
        $chave = $where.uniqid(date('YmdHms'));
        $this->where .= " AND {$where} = :{$chave}";
        $this->cond[$chave] = $cond;
        return $this;
    }
    
    /**
     * Define quais clausulas o query vai ter
     * 
     * @param string $where - Coluna a ser feita na clausula
     * @param string $cond - Condicao da clausula
     * @return \CrudPDO
     */
    public function orWhere($where, $cond){
        $chave = $where.uniqid(date('YmdHms'));
        $this->where .= " OR {$where} = :{$chave}";
        $this->cond[$chave] = $cond;
        return $this;
    }
    
    /**
     * Define quais tabelas se ligarão
     * 
     * @param string $tabelaJoin - Tabela a ser ligada
     * @param string $whereJoin - Campos a serem comparados
     * @return \CrudPDO
     */
    public function join($tableJoin, $whereJoin){
        $this->join .= " JOIN {$tableJoin} ON {$whereJoin} ";
        return $this;
    }
    
    /**
     * Define quais por quais colunas serão ordenadas e qual ordenação
     * 
     * @param string $whereOrder - Colunas a serem ordenadas
     * @param string $order - Qual tipo de ordenação, por padrão DESC
     * @return \CrudPDO
     */
    public function orderBy($whereOrder, $order = 'DESC'){
        $this->orderBy = "ORDER BY {$whereOrder} {$order}";
        return $this;
    }
    
     /**
     * Define qual o limite de registros vai ter essa consulta
     * 
     * @param int $limit - Limite de colunas
     * @return \CrudPDO
     */    
    public function limit($limit){
        $this->limit = "LIMIT {$limit}";
        return $this;
    }
    
    
    /**
     * Define qual o limite de registros vai ter essa consulta
     * 
     * @param int $limit - Limite de colunas
     * @return \CrudPDO
     */    
    public function distinct(){
        $this->distinct = 'DISTINCT';
        return $this;
    }
    
    /**
     * Define quais tabelas se no relacionamento muitos para muitos com a tabela pivo
     * 
     * @param string $tablePivo - Tabela Pivo
     * @param string $idTable - Primeiro id da tabela pivo
     * @param string $tableData - Tabela a ser ligada
     * @param string $idTableData - Segundo Id da tabela pivo
     * @return \CrudPDO
     */
    public function manyToMany($tablePivo, $idTable, $tableData, $idTableData){
        $this->join = "JOIN {$tablePivo} ON {$tablePivo}.{$idTable} = {$this->table}.id "
        . "JOIN {$tableData} ON {$tablePivo}.{$idTableData} = {$tableData}.id";
        return $this;
        
        }

    /**
     * Faz a consulta
     * 
     * @return Fetch Object
     */
    public function get(){
        //$sql = "SELECT {$this->distinct} {$this->select} FROM {$this->table} {$this->join} WHERE {$this->where} {$this->orderBy} {$this->limit}";
        //echo $sql.'<br>';
        
        $data = $this->conn->prepare("SELECT {$this->distinct} {$this->select} FROM {$this->table} {$this->join} WHERE {$this->where} {$this->orderBy} {$this->limit}");
        
        if(isset($this->cond) && count($this->cond) > 0){
            foreach ($this->cond as $key => $value){
                $data->bindValue(":{$key}", $value);
            }
        }
        $data->execute();
        $this->total = $data->rowCount();
        return $data->fetchAll(PDO::FETCH_OBJ);
    }
    
    public function getJson(){
        //$sql = "SELECT {$this->distinct} {$this->select} FROM {$this->table} {$this->join} WHERE {$this->where} {$this->orderBy} {$this->limit}";
        //echo $sql.'<br>';
        
        $data = $this->conn->prepare("SELECT {$this->distinct} {$this->select} FROM {$this->table} {$this->join} WHERE {$this->where} {$this->orderBy} {$this->limit}");
        
        if(isset($this->cond) && count($this->cond) > 0){
            foreach ($this->cond as $key => $value){
                $data->bindValue(":{$key}", $value);
            }
        }
        $data->execute();
        $this->total = $data->rowCount();
        return $data->fetchAll();
    }
    
    public function total(){
        return $this->total;
    }
    
}
