<?php

class PDOConnect{
    private static $conn = null;

    public static function connect(){
        try{
            if(self::$conn === null){
                self::$conn = new PDO(DNS.':host='.HOST.';dbname='.DB, USER, PASSWORD);
//                self::$conn = new PDO('mysql:host=localhost;dbname=curso-ajax', 'root', 'fcm020964');
            }
            return self::$conn;
        }
        catch (PDOException $e){
            echo "Falha ao conectar com o banco de dados. {$e->getMessage()}";
        }
    }
    
}